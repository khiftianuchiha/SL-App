<?php
	require_once 'crudJenis.php';
	session_start();

	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from jenis_sepeda_lipat order by id_jenis DESC LIMIT 1";
			$cariId = bacaJenis($sql);
			if($cariId != null){
				$id = $cariId[0]['id_jenis']+1;
			}else{
				$id = 1 ;
			}
			$jenis = $_POST['jenis'];
			$point = $_POST['point'];

			$insert = tambahJenis($id, $jenis, $point);
			if($insert > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewJenis.php">';
				//header("Location: ../viewJenis.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewJenis.php">';
				//header("Location: ../viewJenis.php");
			}
		}elseif(isset($_POST['Simpan'])){
			$id = $_POST['id'];
			$jenis = $_POST['jenis'];
			$point = $_POST['point'];

			$update = ubahJenis($id, $jenis, $point);
			if($update > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewJenis.php">';
				//header("Location: ../viewJenis.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewJenis.php">';
				//header("Location: ../viewJenis.php");
			}

		}else{
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
			//header("Location: ../404.html");
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
	}
?>