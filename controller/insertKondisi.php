<?php
	require_once 'crudKondisi.php';
	session_start();

	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from kondisi_sepeda_lipat order by id_kondisi DESC LIMIT 1";
			$cariId = bacaKondisi($sql);
			if($cariId != null){
				$id = $cariId[0]['id_kondisi']+1;
			}else{
				$id = 1 ;
			}
			
			$kondisi = $_POST['kondisi'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);
			if($kondisi==null){
				if($point >100 || $point <1){
					$_SESSION['notifikasi'] = 'Simpan';
					$_SESSION['status'] = 'gagal';
					echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
					//header("Location: ../viewKondisi.php");
				}				
			}else{
				if($point >100 || $point <1){
					$_SESSION['notifikasi'] = 'Simpan';
					$_SESSION['status'] = 'gagal';
					echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
					//header("Location: ../viewKondisi.php");
				}else{
					$insert = tambahKondisi($id, $kondisi, $point, $bobot);
					if($insert > 0){
						$_SESSION['notifikasi'] = 'Simpan';
						$_SESSION['status'] = 'berhasil';
						echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
						//header("Location: ../viewKondisi.php");
					}else{
						$_SESSION['notifikasi'] = 'Simpan';
						$_SESSION['status'] = 'gagal';
						echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
						//header("Location: ../viewKondisi.php");
						
					}
				}
				
			}

			
		}elseif(isset($_POST['Simpan'])){
			$id = $_POST['id'];
			$kondisi = $_POST['kondisi'];
			$point = $_POST['point'];
			$ubah = ubahKondisi($id,$kondisi,$point, $bobot);
			echo "Update kondisi_sepeda_lipat SET kondisi = '$kondisi', point = $point, bobot=$bobot where id_kondisi=$id";
			
			
			if($ubah>0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
				//header("Location: ../viewKondisi.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
				//header("Location: ../viewKondisi.php");
				
			}
		}else{
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewKondisi.php">';
			//header("Location: ../viewKondisi.php");
			$_SESSION['status'] = 'gagal';
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
		
	}	
?>