<?php
	require_once 'crudSepedaLipat.php';
	require_once 'crudMerk.php';
	require_once 'crudKondisi.php';
	require_once 'crudTipeRem.php';
	require_once 'crudHarga.php';
	session_start();

	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from sepeda_lipat order by id_jenis DESC LIMIT 1";
			$cariId = bacaSepedaLipat($sql);
			if($cariId != null){
				$id = $cariId[0]['id_sepeda_lipat']+1;
			}else{
				$id = 1 ;
			}
			$nama = $_POST['nama'];
			$merk = $_POST['merk'];
			$jenis = $_POST['jenis'];
			$kondisi = $_POST['kondisi'];
			$tipe_rem = $_POST['tipe_rem'];
			$harga = $_POST['harga'];
			$warna = $_POST['warna'];
			$thn_klrn = $_POST['tahun_keluaran'];

			//mencari point dari kriteria yang akan di masukan kedalam rating
			//kriteria yang dipilih yang hanya kondisi,merk,tipe rem, dan harga
			$cariPointMerk = cariMerk($merk);
			if($cariPointMerk != null){
				$pointMerk = $cariPointMerk[0]['point'];
				$bobotMerk = $cariPointMerk[0]['bobot'];
			}
			$cariPointKondisi = cariKondisi($kondisi);
			if($cariPointKondisi != null){
				$pointKondisi = $cariPointKondisi[0]['point'];
				$bobotKondisi = $cariPointKondisi[0]['bobot'];
			}
			$cariPointTipeRem = cariTipeRem($tipe_rem);
			if($cariPointTipeRem != null){
				$pointTipeRem = $cariPointTipeRem[0]['point'];
				$bobotTipeRem = $cariPointTipeRem[0]['bobot'];
			}
			$cariPointHarga = cariHarga($harga);
			if($cariPointHarga != null){
				$pointHarga = $cariPointHarga[0]['point'];
				$bobotHarga = $cariPointHarga[0]['bobot'];
			}

			$rating = number_format(((($pointMerk*$bobotMerk)+($pointHarga*$bobotHarga)+($pointKondisi*$bobotKondisi)+($pointTipeRem*$bobotTipeRem))/4)*10,2);	
			
			$insert = tambahSepedaLipat($id, $nama, $jenis, $merk, $kondisi, $tipe_rem, $harga,$warna,$thn_klrn,$rating);
			echo "tambahSepedaLipat($id, $nama, $jenis, $merk, $kondisi, $tipe_rem, $harga,$warna,$thn_klrn,$rating)";
			if($insert > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php">';
				//header("Location: ../index.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php">';
				//header("Location: ../index.php");
			}
		}elseif(isset($_POST['Simpan'])){
			$id = $_POST['id'];
			$nama = $_POST['nama'];
			$merk = $_POST['merk'];
			$jenis = $_POST['jenis'];
			$kondisi = $_POST['kondisi'];
			$tipe_rem = $_POST['tipe_rem'];
			$harga = $_POST['harga'];
			$warna = $_POST['warna'];
			$thn_klrn = $_POST['tahun_keluaran'];

			//mencari point dari kriteria yang akan di masukan kedalam rating
			//kriteria yang dipilih yang hanya kondisi,merk,tipe rem, dan harga
			$cariPointMerk = cariMerk($merk);
			if($cariPointMerk != null){
				$pointMerk = $cariPointMerk[0]['point'];
				$bobotMerk = $cariPointMerk[0]['bobot'];
			}
			$cariPointKondisi = cariKondisi($kondisi);
			if($cariPointKondisi != null){
				$pointKondisi = $cariPointKondisi[0]['point'];
				$bobotKondisi = $cariPointKondisi[0]['bobot'];
			}
			$cariPointTipeRem = cariTipeRem($tipe_rem);
			if($cariPointTipeRem != null){
				$pointTipeRem = $cariPointTipeRem[0]['point'];
				$bobotTipeRem = $cariPointTipeRem[0]['bobot'];
			}
			$cariPointHarga = cariHarga($harga);
			if($cariPointHarga != null){
				$pointHarga = $cariPointHarga[0]['point'];
				$bobotHarga = $cariPointHarga[0]['bobot'];
			}

			$rating = number_format(((($pointMerk*$bobotMerk)+($pointHarga*$bobotHarga)+($pointKondisi*$bobotKondisi)+($pointTipeRem*$bobotTipeRem))/4)*10,2);	


			$update = ubahSepedaLipat($id, $nama, $jenis, $merk, $kondisi, $tipe_rem, $harga,$warna,$thn_klrn,$rating); 
			echo "Update sepeda_lipat SET nama_sepeda_lipat = '$nama', id_jenis = $jenis, id_merk = $merk, id_kondisi =$kondisi, id_tipe_rem = $tipe_rem, id_harga =$harga, warna = '$warna',tahun_keluaran =$thn_klrn , rating='$rating' where id_sepeda_lipat=$id";
			if($update>0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php">';
				//header("Location: ../index.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../index.php">';
				//header("Location: ../index.php");
			}
		}else{
			echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
			//header("Location: ../404.html");
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
		
	}
?>