<?php
session_start();
	require_once 'crudHarga.php';

	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from harga_sepeda_lipat order by id_harga DESC LIMIT 1";
			$cariId = bacaHarga($sql);
			if($cariId != null){
				$id = $cariId[0]['id_harga']+1;
			}else{
				$id = 1 ;
			}
			$range_harga = $_POST['range_harga'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$insert = tambahHarga($id, $range_harga, $point,$bobot);
			if($insert > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewHarga.php">';
				//header("Location: ../viewHarga.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewHarga.php">';
				//header("Location: ../viewHarga.php");
			}
		}elseif(isset($_POST['Simpan'])){ //edit
			$id = $_POST['id'];
			$range_harga = $_POST['range_harga'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$update = ubahHarga($id, $range_harga, $point, $bobot);
			if($update > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewHarga.php">';
				//header("Location: ../viewHarga.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewHarga.php">';
				//header("Location: ../viewHarga.php");
			}
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
	}
?>