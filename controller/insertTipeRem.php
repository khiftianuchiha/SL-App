<?php
	require_once 'crudTipeRem.php';
	session_start();
	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from tipe_rem_sepeda_lipat order by id_tipe_rem DESC LIMIT 1";
			$cariId = bacaTipeRem($sql);
			if($cariId != null){
				$id = $cariId[0]['id_tipe_rem']+1;
			}else{
				$id = 1 ;
			}
			$tipe_rem = $_POST['tipe_rem'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$insert = tambahTipeRem($id, $tipe_rem, $point,$bobot);
			if($insert > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewTipeRem.php">';	
				//header("Location: ../viewTipeRem.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';	
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewTipeRem.php">';	
				//header("Location: ../viewTipeRem.php");
			}
		}elseif(isset($_POST['Simpan'])){
			$id = $_POST['id'];
			$tipe_rem = $_POST['tipe_rem'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$update = ubahTipeRem($id, $tipe_rem, $point,$bobot);
			if($update > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewTipeRem.php">';	
				//header("Location: ../viewTipeRem.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewTipeRem.php">';				
				//header("Location: ../viewTipeRem.php");
			}
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
	}
?>