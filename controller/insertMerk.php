<?php
session_start();
	require_once 'crudMerk.php';

	if(isset($_POST)){
		if(isset($_POST['Tambah'])){
			$id = 1;
			$sql = "SELECT * from merk_sepeda_lipat order by id_merk DESC LIMIT 1";
			$cariId = bacaMerk($sql);
			if($cariId != null){
				$id = $cariId[0]['id_merk']+1;
			}else{
				$id = 1 ;
			}
			$kondisi = $_POST['merk'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$insert = tambahMerk($id, $kondisi, $point,$bobot);
			if($insert > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewMerk.php">';
				//header("Location: ../viewMerk.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewMerk.php">';
				//header("Location: ../viewMerk.php");
			}
		}elseif(isset($_POST['Simpan'])){
			$id = $_POST['id'];
			$merk = $_POST['merk'];
			$point = $_POST['point'];
			$bobot = number_format($_POST['bobot'],2);

			$update = ubahMerk($id,$merk,$point,$bobot);
			
			if($update > 0){
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'berhasil';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewMerk.php">';
				//header("Location: ../viewMerk.php");
			}else{
				$_SESSION['notifikasi'] = 'Simpan';
				$_SESSION['status'] = 'gagal';
				echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../viewMerk.php">';
				//header("Location: ../viewMerk.php");
			}
		}

	}else{
		echo '<META HTTP-EQUIV="Refresh" Content="0; URL=../404.html">';
		//header("Location: ../404.html");
	}
?>