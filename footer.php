                <footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted">STMIK AKAKOM YOGYAKARTA - 2021</div>
                        </div>
                    </div>
                </footer>
            
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
        <script src="assets/demo/chart-area-demo.js"></script>
        <script src="assets/demo/chart-bar-demo.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
        <script src="js/datatables-simple-demo.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.js"></script>
        <script src="assets/sweetalert2/sweetalert2.all.min.js"></script>
        <?php
if(isset($_SESSION['status'])){
    $status = $_SESSION['status'];
    $notifikasi = $_SESSION['notifikasi'];
    if($status == "berhasil"){
        ?>
        <div class="alert alert-success" role="alert" style="margin-top:3%">
           Data Berhasil di <?php echo $notifikasi ?>!
        </div>

        <?php
        echo "<script>window.Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Data Berhasil di $notifikasi',
            showConfirmButton: false,
            timer: 15000
        });</script>";
        $_SESSION['status']="";
    }elseif($status == "gagal"){
        ?>
        <div class="alert alert-danger" role="alert" style="margin-top:3%">
           Data Gagal di  <?php echo $notifikasi ?>!
        </div>

        <?php
        echo "<script>window.Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'Data Gagal di $notifikasi',
            showConfirmButton: false,
            timer: 15000
        });</script>";
        $_SESSION['status']="";

    }
}
?>
    </body>
</html>
<script>
    window.setTimeout(function() {
      $(".alert").fadeTo(400, 0).slideUp(400, function(){
          $(this).remove(); 
      });
      }, 5000);
</script>

